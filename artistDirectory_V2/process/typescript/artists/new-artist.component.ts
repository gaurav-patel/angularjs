import { Component } from 'angular2/core';
import { HttpService } from '../HttpService';
import { Artist } from './artist';


@Component({
	template: `

		<h1>Create Artist</h1>

		<form class="search" >
			<label>Name: </label>
			<input name="name" placeholder="type in full name" ng-model="artist.name"> <br />

			<label>Reknown: </label>
			<input name="reknown" placeholder="reknown as" ng-model="artist.reknown"> <br />

			<label>Upload image: </label>
			<input type="file" name="image" ng-model="artist.shortname"> <br />

			<label>Bio: </label> <br />
			<textarea rows="8" cols="50" name="bio" placeholder="Bio" ng-model="artist.bio"> </textarea> <br />

<!--			<button class="btn" (click)="addArtist(name.value, reknown.value, file, bio.value)">Create</button> -->
			<button class="btn" (click)="addArtist()">Create</button>
		</form>
	`,
	styleUrls: ['css/app.css'],
	providers: [ HttpService ]

})

export class NewArtistComponent {
	artist : Artist;

	constructor(private httpService: HttpService){	
	}
	
	addArtist() {

		var uploadUrl = "images/";
		/*
		this.artist.name = name;
		this.artist.reknown = reknown;
		this.artist.bio = bio;
		this.artist.shortname = file;
		*/
		this.httpService.postData(uploadUrl, this.artist);
	}
}