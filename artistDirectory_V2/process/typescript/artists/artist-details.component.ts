import {Component} from 'angular2/core';
import { Artist } from './artist';

@Component({
	selector: 'artist-details',
	template: `
		<section class="card artistinfo">
			<div class="artist cf">
				<h1>{{ artist.name }}</h1>

				<div class="info">
					<h3>{{ artist.reknown }}</h3>
					<img src="images/{{ artist.shortname }}_tn.jpg" alt="{{ artist.name }} photo">

					<div class="longbio">
						{{ artist.bio }}				
					</div> <!-- end of div class= longbio -->

				</div> <!-- end of div class= info -->
			
			</div> <!-- end of div class= artist cf -->
		</section> <!-- end of section class= card artistinfo -->
	`,
	inputs: ['artist'], //reciving individual artist info from parent component
	styleUrls: ['css/artist-details.css']

})

export class ArtistDetailsComponent {

}