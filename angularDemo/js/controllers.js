var myApp = angular.module('myApp', []);

myApp.controller('MyController', function($scope, $http) {
	$http.get("js/MOCK_DATA.json").success( function(data) {
		$scope.people = data;	
	});
	 			
});

myApp.filter('ageFilter', function(){
    return function(birthday){
        var birthday = new Date(birthday);
        var today = new Date();
        var age = ((today - birthday) / (31557600000));
        var age = Math.floor( age );
        return age;
    }
});