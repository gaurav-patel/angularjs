System.register(['angular2/core', '../HttpService'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, HttpService_1;
    var NewArtistComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (HttpService_1_1) {
                HttpService_1 = HttpService_1_1;
            }],
        execute: function() {
            NewArtistComponent = (function () {
                function NewArtistComponent(httpService) {
                    this.httpService = httpService;
                }
                NewArtistComponent.prototype.addArtist = function () {
                    var uploadUrl = "images/";
                    /*
                    this.artist.name = name;
                    this.artist.reknown = reknown;
                    this.artist.bio = bio;
                    this.artist.shortname = file;
                    */
                    this.httpService.postData(uploadUrl, this.artist);
                };
                NewArtistComponent = __decorate([
                    core_1.Component({
                        template: "\n\n\t\t<h1>Create Artist</h1>\n\n\t\t<form class=\"search\" >\n\t\t\t<label>Name: </label>\n\t\t\t<input name=\"name\" placeholder=\"type in full name\" ng-model=\"artist.name\"> <br />\n\n\t\t\t<label>Reknown: </label>\n\t\t\t<input name=\"reknown\" placeholder=\"reknown as\" ng-model=\"artist.reknown\"> <br />\n\n\t\t\t<label>Upload image: </label>\n\t\t\t<input type=\"file\" name=\"image\" ng-model=\"artist.shortname\"> <br />\n\n\t\t\t<label>Bio: </label> <br />\n\t\t\t<textarea rows=\"8\" cols=\"50\" name=\"bio\" placeholder=\"Bio\" ng-model=\"artist.bio\"> </textarea> <br />\n\n<!--\t\t\t<button class=\"btn\" (click)=\"addArtist(name.value, reknown.value, file, bio.value)\">Create</button> -->\n\t\t\t<button class=\"btn\" (click)=\"addArtist()\">Create</button>\n\t\t</form>\n\t",
                        styleUrls: ['css/app.css'],
                        providers: [HttpService_1.HttpService]
                    }), 
                    __metadata('design:paramtypes', [HttpService_1.HttpService])
                ], NewArtistComponent);
                return NewArtistComponent;
            }());
            exports_1("NewArtistComponent", NewArtistComponent);
        }
    }
});

//# sourceMappingURL=new-artist.component.js.map
