import { Injectable } from 'angular2/core';
import { Http } from 'angular2/http';
import 'rxjs/add/operator/map'; //need this import otherwise it's producing error


@Injectable()
export class HttpService {
	constructor(private http: Http) {

	}

	getData(url){
		return this.http.get(url)
			.map(res => res.json());
	}

	postData(uploadUrl, data) {
		var fd = new FormData();

		for(var key in data) {
			fd.append(key, data[key]);
		}

		this.http.post(uploadUrl, fd, {
//			transformRequest: angular.indentity,
			headers: {'content-Type' : undefined }
		})
	}
}