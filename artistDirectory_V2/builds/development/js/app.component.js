System.register(['angular2/core', './artists/artist-list.component', './artists/new-artist.component', 'angular2/router'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, artist_list_component_1, new_artist_component_1, router_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (artist_list_component_1_1) {
                artist_list_component_1 = artist_list_component_1_1;
            },
            function (new_artist_component_1_1) {
                new_artist_component_1 = new_artist_component_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent() {
                }
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        template: "\n\t\t<div class=\"artistsearch\">\n\t\t<div class=\"card search\">\n\t\t<header>\n\t\t\t<nav>\n\t\t\t\t<a [routerLink]=\"['Artist']\">Artists</a>\n\t\t\t\t<a [routerLink]=\"['NewArtist']\">New Artist</a>\n\t\t\t</nav>\n\t\t</header>\n\t\t<hr />\n\t\t\n\t\t<router-outlet></router-outlet>\n\t\t</div>\n\t\t</div>\n\t",
                        directives: [router_1.ROUTER_DIRECTIVES],
                        styleUrls: ['css/app.css']
                    }),
                    router_1.RouteConfig([
                        { path: '/artist', name: 'Artist', component: artist_list_component_1.ArtistListComponent, useAsDefault: true },
                        { path: '/newartist', name: 'NewArtist', component: new_artist_component_1.NewArtistComponent }
                    ]), 
                    __metadata('design:paramtypes', [])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});

//# sourceMappingURL=app.component.js.map
