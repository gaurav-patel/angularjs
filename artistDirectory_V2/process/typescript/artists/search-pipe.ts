import { Pipe } from 'angular2/core';

@Pipe({
	name: 'find' //for calling pipe by
})

export class SearchPipe {
	transform(Data, [pipeModifier]) {
		return Data.filter((eachItem) => { 
			return eachItem['name'].toLowerCase().includes(pipeModifier.toLowerCase()) ||
					eachItem['reknown'].toLowerCase().includes(pipeModifier.toLowerCase());
		});
	}
}