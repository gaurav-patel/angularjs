import { Component } from 'angular2/core';
import { Artist } from './artist';
import { SearchPipe } from './search-pipe';
import { HttpService } from '../HttpService';
import { ArtistItemComponent } from './artist-item.component';
import { ArtistDetailsComponent } from './artist-details.component';

@Component({
	selector: 'artist-list',
	template: `
		<div class="artistsearch">
			<div class="card search">
				<h1>Artist Directory</h1>
				<label>search <span *ngIf="query"> for: {{ query }} </span></label>
		<!--		
				<input #newArtist
					(keyup.enter)="addArtist(newArtist.value); newArtist.value=''"
					[(ngModel)]="name"
				>
		-->		
				<input [(ngModel)]="query" placeholder="type in search term">
		<!--		<button class="btn" (click)="addArtist(newArtist.value); newArtist.value=''">Add</button>
		-->
			</div> <!-- end of div class= card search -->

		</div> <!-- end of div class= artistsearch -->

		<ul *ngIf="query" class="artistlist cf">
		<!-- 	
			<li class="artist cf" #artistContainer (click)="onClick(item, artistContainer);" *ngFor="#item of artists">
		-->
			<li class="artist cf" (click)="showArtist(item); query='';" *ngFor="#item of artists | find:query"> <!-- | find:query = calling custom pipe function for filtering data -->
				<artist-item class="content" [artist]=item></artist-item>
			</li>
		</ul> <!-- end of ul class= artistlist cf -->

		<artist-details *ngIf="currentArtist" [artist]="currentArtist"></artist-details>
	`,
	directives: [ArtistItemComponent, ArtistDetailsComponent],
	pipes: [SearchPipe], //including custom search pipes
	providers: [HttpService],
	styleUrls: ['css/app.css']

})

export class ArtistListComponent {
	artists : Artist[];
	currentArtist : Artist;

	constructor(private httpService: HttpService){	
		this.httpService.getData('./js/data.json')
			.subscribe(
				data => { this.artists =  data; },
				error => { console.log(error); },
				() => console.log("Received Artist Data successfully.")
			);
	}

	showArtist(artist) {
		this.currentArtist = artist;
	}
}