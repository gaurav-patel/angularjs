import {Component} from 'angular2/core';

@Component ({
	selector: 'artist-item',
	template: `
		<img src="images/{{ artist.shortname }}_tn.jpg" alt="{{ artist.name }} photo">	
		<div class="info">
			<h2>{{ artist.name }}</h2>
			<p>{{ artist.reknown }}</p>
		</div> <!-- end of div class= info -->
	`,
	inputs: ['artist'], //reciving individual artist info from parent component
	styleUrls: ['css/artist-items.css'] //The css from parent component doen't appy to other content so need to add it additionally
})

export class ArtistItemComponent {

}