import { Component } from 'angular2/core';
import { ArtistListComponent } from './artists/artist-list.component';
import { NewArtistComponent } from './artists/new-artist.component';
import { ROUTER_DIRECTIVES, RouteConfig } from 'angular2/router';

@Component({
	selector: 'my-app',
	template: `
		<div class="artistsearch">
		<div class="card search">
		<header>
			<nav>
				<a [routerLink]="['Artist']">Artists</a>
				<a [routerLink]="['NewArtist']">New Artist</a>
			</nav>
		</header>
		<hr />
		
		<router-outlet></router-outlet>
		</div>
		</div>
	`,
	directives: [ROUTER_DIRECTIVES ],
	styleUrls: ['css/app.css']
})

@RouteConfig([
	{ path: '/artist', name: 'Artist', component: ArtistListComponent, useAsDefault: true },
	{ path: '/newartist', name: 'NewArtist', component: NewArtistComponent }
])

export class AppComponent {

}