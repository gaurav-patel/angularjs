var artistControllers = angular.module('artistControllers', [
	'ngAnimate' //for animations
]);

artistControllers.controller('ListController', ['$scope', '$http', function($scope, $http) {
  $http.get('js/data.json').success(function(data) {
    $scope.artists = data;
    $scope.artistOrder = 'name';
  });
}]);


artistControllers.controller('DetailsController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {
  $http.get('js/data.json').success(function(data) {
    $scope.artists = data[$routeParams.id];
    $scope.artistId = $routeParams.id;

    //Setting nextItem and prevItem values for next and previous links
    if($scope.artistId > 0) {
    	$scope.prevItem = Number($scope.artistId) - 1;
    } else {
    	$scope.prevItem = $scope.artists.length - 1;
    }

    if($scope.artistId < $scope.artists.length - 1) {
    	$scope.nextItem = Number($scope.artistId) + 1;
    } else {
    	$scope.nextItem = 0;
    }

  });
}]);