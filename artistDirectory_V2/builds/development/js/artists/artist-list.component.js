System.register(['angular2/core', './search-pipe', '../HttpService', './artist-item.component', './artist-details.component'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, search_pipe_1, HttpService_1, artist_item_component_1, artist_details_component_1;
    var ArtistListComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (search_pipe_1_1) {
                search_pipe_1 = search_pipe_1_1;
            },
            function (HttpService_1_1) {
                HttpService_1 = HttpService_1_1;
            },
            function (artist_item_component_1_1) {
                artist_item_component_1 = artist_item_component_1_1;
            },
            function (artist_details_component_1_1) {
                artist_details_component_1 = artist_details_component_1_1;
            }],
        execute: function() {
            ArtistListComponent = (function () {
                function ArtistListComponent(httpService) {
                    var _this = this;
                    this.httpService = httpService;
                    this.httpService.getData('./js/data.json')
                        .subscribe(function (data) { _this.artists = data; }, function (error) { console.log(error); }, function () { return console.log("Received Artist Data successfully."); });
                }
                ArtistListComponent.prototype.showArtist = function (artist) {
                    this.currentArtist = artist;
                };
                ArtistListComponent = __decorate([
                    core_1.Component({
                        selector: 'artist-list',
                        template: "\n\t\t<div class=\"artistsearch\">\n\t\t\t<div class=\"card search\">\n\t\t\t\t<h1>Artist Directory</h1>\n\t\t\t\t<label>search <span *ngIf=\"query\"> for: {{ query }} </span></label>\n\t\t<!--\t\t\n\t\t\t\t<input #newArtist\n\t\t\t\t\t(keyup.enter)=\"addArtist(newArtist.value); newArtist.value=''\"\n\t\t\t\t\t[(ngModel)]=\"name\"\n\t\t\t\t>\n\t\t-->\t\t\n\t\t\t\t<input [(ngModel)]=\"query\" placeholder=\"type in search term\">\n\t\t<!--\t\t<button class=\"btn\" (click)=\"addArtist(newArtist.value); newArtist.value=''\">Add</button>\n\t\t-->\n\t\t\t</div> <!-- end of div class= card search -->\n\n\t\t</div> <!-- end of div class= artistsearch -->\n\n\t\t<ul *ngIf=\"query\" class=\"artistlist cf\">\n\t\t<!-- \t\n\t\t\t<li class=\"artist cf\" #artistContainer (click)=\"onClick(item, artistContainer);\" *ngFor=\"#item of artists\">\n\t\t-->\n\t\t\t<li class=\"artist cf\" (click)=\"showArtist(item); query='';\" *ngFor=\"#item of artists | find:query\"> <!-- | find:query = calling custom pipe function for filtering data -->\n\t\t\t\t<artist-item class=\"content\" [artist]=item></artist-item>\n\t\t\t</li>\n\t\t</ul> <!-- end of ul class= artistlist cf -->\n\n\t\t<artist-details *ngIf=\"currentArtist\" [artist]=\"currentArtist\"></artist-details>\n\t",
                        directives: [artist_item_component_1.ArtistItemComponent, artist_details_component_1.ArtistDetailsComponent],
                        pipes: [search_pipe_1.SearchPipe],
                        providers: [HttpService_1.HttpService],
                        styleUrls: ['css/app.css']
                    }), 
                    __metadata('design:paramtypes', [HttpService_1.HttpService])
                ], ArtistListComponent);
                return ArtistListComponent;
            }());
            exports_1("ArtistListComponent", ArtistListComponent);
        }
    }
});

//# sourceMappingURL=artist-list.component.js.map
