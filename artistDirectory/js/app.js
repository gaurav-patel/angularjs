var myApp = angular.module('myApp', [
  'ngRoute', //using ngRoute for deep linking (route path)
  'artistControllers'
]);

//routeProvider is service provided by ngRoute
myApp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.
  when('/list', {
    templateUrl: 'partials/list.html',
    controller: 'ListController'
  }).
  when("/details/:id", {
  	templateUrl: 'partials/details.html',
  	controller: 'DetailsController'
  }).
  otherwise({
    redirectTo: '/list'
  });
}]);