# README #

## Artist Directory V2
Web application builds using AngularJS 2, TypeScript and GulpJS build tool, where I've read data from 
JSON file, maps URL routes to controllers, custom pipe filtering, and used partial views. The 
application has two way data binded between search bar and artist list and upon selecting one, the 
artist full details is shown, also made use events to have animations as well.

![Artist Directory version 2](/artist-directory_v2.png "Artist Directory version 2 screen-shot")

## Artist Directory
Created angular application where I've read data from JSON file, map URL routes to controllers, and 
used partial views. The application displays all the artists on index page and upon selecting one, the 
artist full details is shown. Made use of few angular events to have animations as well. Developed this 
application while following AngularJS course on Lynda.com

![Artist Directory application](/artist-directory.png "Artist Directory application screen-shot")
 
## Angular Demo 

Created my first angular application where I've read data from JSON file and made use of controller and 
directives to display data on index, and used bootstrap for responsive design. I've used data generator tool
to generate dummy data, used that data to display person's profile.

![Angular Demo application](/angular-demo.png "Angular Demo application screen-shot")

